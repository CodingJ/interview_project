//
//  ParkingTableViewCell.swift
//  interviewProject
//
//  Created by 洪正倫 on 1/4/19.
//  Copyright © 2019 Jason_Hung. All rights reserved.
//

import UIKit

class ParkingTableViewCell: UITableViewCell {

    @IBOutlet weak var regionLAbel: UILabel!
    @IBOutlet weak var openingHourLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
}
