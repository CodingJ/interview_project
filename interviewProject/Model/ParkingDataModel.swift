//
//  ParkingDataModel.swift
//  interviewProject
//
//  Created by 洪正倫 on 1/4/19.
//  Copyright © 2019 Jason_Hung. All rights reserved.
//

import Foundation
import RealmSwift

class ParkingDataModel: Object{
    
    @objc dynamic var regionOfParkingLot: String = ""
    @objc dynamic var openingHourOfParkingLot: String = ""
    @objc dynamic var addressOfParkingLot: String = ""
    @objc dynamic var nameOfParkingLot: String = ""

}
