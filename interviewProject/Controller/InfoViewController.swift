//
//  InfoViewController.swift
//  interviewProject
//
//  Created by 洪正倫 on 1/6/19.
//  Copyright © 2019 Jason_Hung. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit


class InfoViewController: UIViewController{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: GMSMapView!
    
    var selectedParkingLot: ParkingDataModel?
    
    private var Lat: Double!
    private var Lon: Double!
   

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "InfoTableViewCell", bundle: nil), forCellReuseIdentifier: "InfoCell")
        
        setAnnotation(with: selectedParkingLot!.addressOfParkingLot)
        
      
        print("View Did Load")
    }
    //MARK -MapView Methods
    func showMarker(position: CLLocationCoordinate2D){
        let marker = GMSMarker()
        marker.position = position
        marker.map = mapView
    }
    
    func setAnnotation(with address: String) {
        print("ADDRESS AT:" + address)
        
         let geoCoder = CLGeocoder()
        
        geoCoder.geocodeAddressString(address) { (placemarks, error) in
            if let error = error {
                print("Error getting Coordinates, \(error)")
                return
            }
            
            if let placemarks = placemarks{
                let placemark = placemarks[0] as CLPlacemark
                self.Lat = placemark.location!.coordinate.latitude
                self.Lon = placemark.location!.coordinate.longitude
                
                let camera = GMSCameraPosition.camera(withLatitude: self.Lat, longitude: self.Lon, zoom: 18.0)
                self.mapView.camera = camera
                self.showMarker(position: camera.target)
            }
        }
    }
    
    
}

//MARK: - Tableview Delegate nad Datasource Methods
extension InfoViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InfoCell", for: indexPath) as! InfoTableViewCell
        
        cell.selectionStyle = .none
        
        switch indexPath.row {
        
        case 0:
            cell.InfoLabel.text = "停車場名稱"
            cell.DataLabel.text = selectedParkingLot?.nameOfParkingLot
        case 1:
            cell.InfoLabel.text = "區域"
            cell.DataLabel.text = selectedParkingLot?.regionOfParkingLot
        case 2:
            cell.InfoLabel.text = "營業時間"
            cell.DataLabel.text = selectedParkingLot?.openingHourOfParkingLot
        case 3:
            cell.InfoLabel.text = "地址"
            cell.DataLabel.text = selectedParkingLot?.addressOfParkingLot
        default:
            print("No data Yet")
        }
        
        return cell
    }
    
}

