//
//  ViewController.swift
//  interviewProject
//
//  Created by 洪正倫 on 1/4/19.
//  Copyright © 2019 Jason_Hung. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import RealmSwift
import SVProgressHUD

class ParkingTableViewController: UITableViewController {
    
    private var parkingInfo: Results<ParkingDataModel>?
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    private let defalts = UserDefaults.standard
    
    private var recordArray = [[String : Any]]()
    
    private let realm = try! Realm()
    
    private let data_URL = "http://data.ntpc.gov.tw/api/v1/rest/datastore/382000000A-000225-002"
    
    private let parkingDataModel = ParkingDataModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tableView.register(UINib(nibName: "ParkingTableViewCell", bundle: nil), forCellReuseIdentifier: "ParkingCell")
        
        let isFirstVisitApp = defalts.bool(forKey: "FirstVisitApp")
       
        if !isFirstVisitApp {
            getData()
        }
        
        loadParkingData()
        
    }
    //MARK -TableView DataSource Methods
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return parkingInfo?.count ?? 1
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ParkingCell", for: indexPath) as! ParkingTableViewCell
        
        if let parkingLot = parkingInfo?[indexPath.row] {
            cell.regionLAbel.text = parkingLot.regionOfParkingLot
            cell.openingHourLabel.text = parkingLot.openingHourOfParkingLot
            cell.addressLabel.text = parkingLot.addressOfParkingLot
            cell.nameLabel.text = parkingLot.nameOfParkingLot
        }
        
        return cell
    }
    
    //MARK: -TableView delegate Methods
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "GoToDetail", sender: self)
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        print(indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! InfoViewController
        
        if let indexPath = tableView.indexPathForSelectedRow{
            destination.selectedParkingLot = parkingInfo?[indexPath.row]
        }
    }
    
    //MARK: -Networking
    
    func getData(){
            SVProgressHUD.show()
            Alamofire.request(data_URL, method: .get).responseJSON { (response) in
                if response.result.isSuccess {
                    print("Success, Got data!")
                    
                    let data = JSON(response.result.value!)
                    
                    print(data)
                    
                    self.updateData(json: data)
                    
                    self.defalts.set(true, forKey: "FirstVisitApp")
                    
                } else {
                    print(print("Error \(String(describing: response.result.error))"))
                }
            }
    }
    
    func updateData(json: JSON){
        
        
        if let records = json["result"]["records"].arrayObject as? [[String : Any]]{
            for record in records {
                recordArray.append(record)
            }
        }
        
        for i in 0 ... recordArray.count - 1 {
            
            let parkingDataModel = ParkingDataModel()
            
            parkingDataModel.regionOfParkingLot = recordArray[i]["AREA"] as! String
            
            parkingDataModel.openingHourOfParkingLot = recordArray[i]["SERVICETIME"] as! String
            
            parkingDataModel.addressOfParkingLot = recordArray[i]["ADDRESS"] as! String
            
            parkingDataModel.nameOfParkingLot = recordArray[i]["NAME"] as! String
            
            save(data: parkingDataModel)
            
            SVProgressHUD.dismiss()

        }
                print("Count: \(recordArray.count)")
        
    }
    
    //MARK: -Data munipulate Functions
    
    func save(data: ParkingDataModel){
        do {
            try realm.write {
                realm.add(data)
            }
        } catch{
            print("Error saving categories, \(error)")
        }
        
        tableView.reloadData()
        
    }
    
    func loadParkingData(){
        
        parkingInfo = realm.objects(ParkingDataModel.self)
        
        tableView.reloadData()
    }
    
}

extension ParkingTableViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        //predicate : specify how we want to query our database
        if searchBar.text?.trimmingCharacters(in: .whitespaces).isEmpty == false{
            parkingInfo = parkingInfo?.filter("nameOfParkingLot CONTAINS[cd] %@", searchBar.text!)
            .filter("regionOfParkingLot CONTAINS[cd] %@", searchBar.text!)
            tableView.reloadData()
        }
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.count == 0 {
            
            loadParkingData()
            
            DispatchQueue.main.async {
                searchBar.resignFirstResponder()
            }
        }
    }
}

